package fapfood.constitution;


import fapfood.constitution.PartsOfConstitution.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by FAPFOOD on 2016-11-30.
 */
public class ConstitutionParser {
    private String constitutionBeginning = ".?KONSTYTUCJA.*";
    private String preambleBeginning = "Preambuła.*";
    private String titleBeginning = "[A-ZĄĆĘŁŃÓŚŹŻ]+";
    private String chapterBeginning = "Rozdział .*"; //Rozdział I
    private String articleBeginning = "Art\\. [0-9]+\\.?.*";
    private String paragraphBeginning = "[0-9]+\\. .*";
    private String subparagraphBeginning = "[0-9]+\\) .*";
    private String currentLine;
    private BufferedReader bufferedReader;


    public Constitution readFile(String source) throws IOException {
        FileReader reader = new FileReader(source + "\\konstytucja2.txt");
        this.bufferedReader = new BufferedReader(reader);
        this.currentLine = this.bufferedReader.readLine();
        return this.constitutionMatches();
    }

    private Constitution constitutionMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.constitutionBeginning, this.currentLine)) {

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(this.currentLine);

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)
                        && !Pattern.matches(this.preambleBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String content = stringBuilder.toString();

            Preamble preamble = this.preambleMatches();
            Constitution constitution = new Constitution(preamble);

            Chapter chapter;
            while (true) {
                chapter = this.chapterMatches();
                if (chapter != null)
                    constitution.addChapter(chapter);
                else
                    break;
            }
            return constitution;
        } else
            return null;
    }

    private Preamble preambleMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.preambleBeginning, this.currentLine)) {

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(this.currentLine);

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.articleBeginning, this.currentLine)
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)
                        && !Pattern.matches(this.preambleBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String content = stringBuilder.toString();
            return new Preamble(content);

        } else
            return null;
    }

    private Chapter chapterMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.chapterBeginning, this.currentLine)) {

            RomanNumerals romanNumber = RomanNumerals.toEnum(this.currentLine.replace("Rozdział ", ""));

            StringBuilder stringBuilder = new StringBuilder();

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.articleBeginning, this.currentLine)
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String chapterTitle = stringBuilder.toString();
            Chapter chapter = new Chapter(romanNumber, chapterTitle);

            Title previousTitle = null;
            Title title = null;
            Article article;
            while (true) {
                if (this.currentLine != null
                        && Pattern.matches(this.titleBeginning, this.currentLine)) {
                    previousTitle = title;
                    title = new Title(this.currentLine);
                }
                article = this.articleMatches();
                if (article != null)
                    chapter.addArticle(article);
                else
                    break;
            }

            return chapter;
        } else
            return null;
    }

    private Article articleMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.articleBeginning, this.currentLine)) {

            this.currentLine = this.currentLine.replace("Art. ", "").replace(".", "");
            int number = Integer.parseInt(this.currentLine);

            StringBuilder stringBuilder = new StringBuilder();

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.paragraphBeginning, this.currentLine)
                        && !Pattern.matches(this.articleBeginning, this.currentLine)
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String content = stringBuilder.toString();
            Article article = new Article(number, content);

            Paragraph paragraph;
            while (true) {
                paragraph = this.paragraphMatches();
                if (paragraph != null)
                    article.addParagraph(paragraph);
                else
                    break;
            }

            return article;
        } else
            return null;
    }

    private Paragraph paragraphMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.paragraphBeginning, this.currentLine)) {

            int number = Integer.parseInt(this.currentLine.substring(0, this.currentLine.indexOf(".")));

            this.currentLine = this.currentLine.replaceFirst("[0-9]+\\. ", "");

            StringBuilder stringBuilder = new StringBuilder();

            this.linkLine();
            stringBuilder.append(this.currentLine);

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.subparagraphBeginning, this.currentLine)
                        && !Pattern.matches(this.paragraphBeginning, this.currentLine)
                        && !Pattern.matches(this.articleBeginning, this.currentLine)
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String content = stringBuilder.toString();
            Paragraph paragraph = new Paragraph(number, content);

            Subparagraph subparagraph;
            while (true) {
                subparagraph = this.subparagraphMatches();
                if (subparagraph != null)
                    paragraph.addSubparagraph(subparagraph);
                else
                    break;
            }

            return paragraph;
        } else
            return null;
    }

    private Subparagraph subparagraphMatches() throws IOException {
        if (this.currentLine != null && Pattern.matches(this.subparagraphBeginning, this.currentLine)) {

            int number = Integer.parseInt(this.currentLine.substring(0, this.currentLine.indexOf(")")));

            this.currentLine = this.currentLine.replaceFirst("[0-9]+\\) ", "");

            StringBuilder stringBuilder = new StringBuilder();

            this.linkLine();
            stringBuilder.append(this.currentLine);

            while (true) {
                this.currentLine = this.bufferedReader.readLine();
                this.skipCopyright();
                this.skipDate();
                if (this.currentLine != null
                        && !Pattern.matches(this.subparagraphBeginning, this.currentLine)
                        && !Pattern.matches(this.paragraphBeginning, this.currentLine)
                        && !Pattern.matches(this.articleBeginning, this.currentLine)
                        && !Pattern.matches(this.chapterBeginning, this.currentLine)) {
                    this.linkLine();
                    stringBuilder.append(this.currentLine);
                } else
                    break;
            }

            String content = stringBuilder.toString();
            return new Subparagraph(number, content);
        } else
            return null;
    }

    private void linkLine() {
        if (this.currentLine != null && this.currentLine.endsWith("-")) {
            this.currentLine = this.currentLine.substring(0, this.currentLine.lastIndexOf("-"));
        } else
            this.currentLine = this.currentLine + " ";
    }

    private void skipCopyright() throws IOException {
        String copyright = "\u00a9Kancelaria Sejmu"; //©Kancelaria Sejmu
        if (this.currentLine != null && Pattern.matches(copyright, this.currentLine)) {
            this.currentLine = this.bufferedReader.readLine();
        }
    }

    private void skipDate() throws IOException {
        String date = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
        if (this.currentLine != null && Pattern.matches(date, this.currentLine)) {
            this.currentLine = this.bufferedReader.readLine();
        }
    }
}
