package fapfood.constitution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FAPFOOD on 2016-12-09.
 */
public class CommandLineArgumentsParser {
    private String source;
    private List<Integer> listOfArticleNumbers = new ArrayList<>();
    private List<RomanNumerals> listOfChapterNumbers = new ArrayList<>();
    private boolean printConstitution = false;

    public void parse(String[] args) {
        int articleNumber;
        RomanNumerals chapterNumber;

        for (int i = 0; i < args.length - 1; i++) {
            switch (args[i]) {
                case "-a":
                    i++;
                    while (args.length - 1 >= i && !args[i].equals("-c") && !args[i].equals("-s")) {
                        try {
                            articleNumber = Integer.valueOf(args[i]);
                        } catch (NumberFormatException e) {
                            i++;
                            continue;
                        }
                        listOfArticleNumbers.add(articleNumber);
                        i++;
                    }
                    break;

                case "-c":
                    i++;
                    while (args.length - 1 >= i && !args[i].equals("-a") && !args[i].equals("-s")) {
                        chapterNumber = RomanNumerals.toEnum(args[i]);
                        if (chapterNumber != null) {
                            listOfChapterNumbers.add(chapterNumber);
                        }
                        i++;
                    }
                    break;

                case "-s":
                    if (args.length >= ++i)
                        this.source = args[i];
                    break;
                default:
                    break;
            }
        }
    }

    public String getSource() {
        return this.source;
    }

    public List<Integer> getArticles() {
        return this.listOfArticleNumbers;
    }

    public List<RomanNumerals> getChapters() {
        return this.listOfChapterNumbers;
    }

    public boolean printConstitution() {
        return this.printConstitution;
    }
}

