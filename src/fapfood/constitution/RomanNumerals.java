package fapfood.constitution;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public enum RomanNumerals {
    I, II, III, IV, V, VI, VII, VIII, IX, X, XI, XII, XIII, XIV, XV;

    public static RomanNumerals toEnum(String number) {
        switch (number) {
            case "I":
                return RomanNumerals.I;
            case "II":
                return RomanNumerals.II;
            case "III":
                return RomanNumerals.III;
            case "IV":
                return RomanNumerals.IV;
            case "V":
                return RomanNumerals.V;
            case "VI":
                return RomanNumerals.VI;
            case "VII":
                return RomanNumerals.VII;
            case "VIII":
                return RomanNumerals.VIII;
            case "IX":
                return RomanNumerals.IX;
            case "X":
                return RomanNumerals.X;
            case "XI":
                return RomanNumerals.XI;
            case "XII":
                return RomanNumerals.XII;
            case "XIII":
                return RomanNumerals.XIII;
            case "XIV":
                return RomanNumerals.XIV;
            case "XV":
                return RomanNumerals.XV;
            default:
                return null;
        }
    }

}
