package fapfood.constitution;

import fapfood.constitution.PartsOfConstitution.Article;
import fapfood.constitution.PartsOfConstitution.Constitution;

import java.io.IOException;

/**
 * Created by FAPFOOD on 2016-12-02.
 */
public class Main {

    public static void main(String[] args) {
        CommandLineArgumentsParser claParser = new CommandLineArgumentsParser();
        ConstitutionParser consParser = new ConstitutionParser();
        claParser.parse(args);
        try {
            Constitution constitution = consParser.readFile(claParser.getSource());
            Printer printer = new Printer(claParser, constitution);
            printer.print();
        } catch (IOException e) {
            System.out.println("Plik: " + claParser.getSource() + "\\konstytucja.txt nie istnieje, być może podałeś złą lokalizację");
        }
    }
}
