package fapfood.constitution.PartsOfConstitution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FAPFOOD on 2016-11-30.
 */
public class Article {
    private int number;
    private String content;
    private List<Paragraph> listsOfParagraphs = new ArrayList<>();

    public Article(int number, String content) {
        this.number = number;
        this.content = content;
    }

    public void addParagraph(Paragraph paragraph) {
        this.listsOfParagraphs.add(paragraph);
    }

    public int getNumber() {
        return this.number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        return getNumber() == article.getNumber();
    }

    @Override
    public int hashCode() {
        return getNumber();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        boolean contentIsEmpty = this.content.equals(""); //jeśli pełne false

        stringBuilder.append("Art. ");
        stringBuilder.append(this.getNumber());
        stringBuilder.append(".");
        stringBuilder.append(System.lineSeparator());

        if (!contentIsEmpty) { // jeśli pełne true
            stringBuilder.append(this.content);
        }

        for (Paragraph paragraph : this.listsOfParagraphs) {
            if (!contentIsEmpty) { // jeśli pełne true
                stringBuilder.append(System.lineSeparator());
            }
            contentIsEmpty = false; // jeśli pełne zawsze false

            stringBuilder.append(paragraph.getNumber());
            stringBuilder.append(". ");
            stringBuilder.append(paragraph.toString());
        }
        return stringBuilder.toString();
    }
}
