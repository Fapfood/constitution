package fapfood.constitution.PartsOfConstitution;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public class Subparagraph {
    private int number;
    private String content;

    public Subparagraph(int number, String content) {
        this.number = number;
        this.content = content;
    }

    public int getNumber() {
        return this.number;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
