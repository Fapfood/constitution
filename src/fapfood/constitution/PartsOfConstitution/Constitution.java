package fapfood.constitution.PartsOfConstitution;

import fapfood.constitution.RomanNumerals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public class Constitution {
    private Preamble preamble;
    private List<Chapter> listOfChapters = new ArrayList<>();
    private Map<RomanNumerals, Chapter> mapOfChapters = new HashMap<>();
    private Map<Integer, Article> mapOfArticles = new HashMap<>();

    public Constitution(Preamble preamble) {
        this.preamble = preamble;
    }

    public void addChapter(Chapter chapter) {
        this.listOfChapters.add(chapter);
        this.mapOfChapters.put(chapter.getNumber(), chapter);
        for (Article article : chapter.getListOfArticles()) {
            this.mapOfArticles.put(article.getNumber(), article);
        }
    }

    public Chapter getChapter(RomanNumerals number) {
        return this.mapOfChapters.get(number);
    }

    public Article getArticle(int number) {
        return this.mapOfArticles.get(number);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(this.preamble.toString());
        stringBuilder.append(System.lineSeparator());
        boolean first = true;
        for (Chapter chapter : this.listOfChapters) {
            if (chapter != null) {
                if (!first) {
                    stringBuilder.append(System.lineSeparator());
                }
                first = false;
                stringBuilder.append("Rozdział ");
                stringBuilder.append(chapter.getNumber());
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append(chapter.toString());
            }
        }
        return stringBuilder.toString();
    }
}
