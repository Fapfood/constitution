package fapfood.constitution.PartsOfConstitution;

import fapfood.constitution.RomanNumerals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by FAPFOOD on 2016-11-30.
 */
public class Chapter {
    private RomanNumerals number;
    private String chapterTitle;
    private List<Article> listOfArticles = new ArrayList<>();
    private Map<Integer, Article> mapOfArticles = new HashMap<>();
    private List<Title> listOfTitles = new ArrayList<>();

    public Chapter(RomanNumerals number, String chapterTitle) {
        this.number = number;
        this.chapterTitle = chapterTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chapter chapter = (Chapter) o;

        return getNumber() == chapter.getNumber();
    }

    @Override
    public int hashCode() {
        return getNumber().hashCode();
    }

    public void addArticle(Article article) {
        this.listOfArticles.add(article);
        this.mapOfArticles.put(article.getNumber(), article);

    }

    public void addTitle(Title title) {
        this.listOfTitles.add(title);
    }

    public RomanNumerals getNumber() {
        return this.number;
    }

    public Article getArticle(int number) {
        return this.mapOfArticles.get(number);
    }

    public List<Article> getListOfArticles() {
        return this.listOfArticles;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.chapterTitle);

        for (Article article : this.listOfArticles) {
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(article.toString());
        }
        return stringBuilder.toString();
    }
}
