package fapfood.constitution.PartsOfConstitution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public class Paragraph {
    private int number;
    private String content;
    private List<Subparagraph> listsOfSubparagraphs = new ArrayList<>();

    public Paragraph(int number, String content) {
        this.number = number;
        this.content = content;
    }

    public void addSubparagraph(Subparagraph subparagraph) {
        this.listsOfSubparagraphs.add(subparagraph);
    }

    public int getNumber() {
        return this.number;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.content);

        for (Subparagraph subparagraph : this.listsOfSubparagraphs) {
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(subparagraph.getNumber());
            stringBuilder.append(") ");
            stringBuilder.append(subparagraph.toString());
        }
        return stringBuilder.toString();
    }
}
