package fapfood.constitution.PartsOfConstitution;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public class Title {
    private String title;
    private Article startOfTheRangeOfArticles;
    private Article endOfTheRangeOfArticles;

    public Title(String title) {
        this.title = title;
    }

    public void setStartOfTheRangeOfArticles(Article startOfTheRangeOfArticles) {
        this.startOfTheRangeOfArticles = startOfTheRangeOfArticles;
    }

    public void setEndOfTheRangeOfArticles(Article endOfTheRangeOfArticles) {
        this.endOfTheRangeOfArticles = endOfTheRangeOfArticles;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
