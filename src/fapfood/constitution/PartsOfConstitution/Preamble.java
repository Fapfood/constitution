package fapfood.constitution.PartsOfConstitution;

/**
 * Created by FAPFOOD on 2016-12-06.
 */
public class Preamble {
    private String content;

    public Preamble(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.content);
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
