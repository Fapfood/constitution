package fapfood.constitution;

import fapfood.constitution.PartsOfConstitution.Constitution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FAPFOOD on 2016-12-09.
 */
public class Printer {
    private Constitution constitution;
    private List<Integer> listOfArticleNumbers = new ArrayList<>();
    private List<RomanNumerals> listOfChapterNumbers = new ArrayList<>();

    public Printer(CommandLineArgumentsParser claParser, Constitution constitution) {
        this.constitution = constitution;
        this.listOfArticleNumbers = claParser.getArticles();
        this.listOfChapterNumbers = claParser.getChapters();
    }


    public void print() {

        if (this.listOfArticleNumbers.size() > 0 && this.listOfArticleNumbers.get(0) != null)
            if (constitution.getArticle(this.listOfArticleNumbers.get(0)) != null)
                if (this.listOfArticleNumbers.size() > 1 && this.listOfArticleNumbers.get(1) != null
                        && constitution.getArticle(this.listOfArticleNumbers.get(1)) != null)
                    if (this.listOfArticleNumbers.get(0) < this.listOfArticleNumbers.get(1))
                        for (int i = this.listOfArticleNumbers.get(0); i <= this.listOfArticleNumbers.get(1); i++)
                            System.out.println(constitution.getArticle(i).toString());
                    else
                        for (int i = this.listOfArticleNumbers.get(1); i >= this.listOfArticleNumbers.get(0); i--)
                            System.out.println(constitution.getArticle(i).toString());
                else
                    System.out.println(constitution.getArticle(this.listOfArticleNumbers.get(0)).toString());
            else
                System.out.println("Artykuł poza zakresem");

        if (this.listOfChapterNumbers.size() > 0)
            for (RomanNumerals chapterNumber : this.listOfChapterNumbers) {
                if (chapterNumber != null && constitution.getChapter(chapterNumber) != null)
                    System.out.println(constitution.getChapter(chapterNumber).toString());
                else
                    System.out.println("Rozdział poza zakresem");
            }
    }
}
